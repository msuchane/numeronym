# numeronym

A command-line utility that expands a [numeronym](https://en.wikipedia.org/wiki/Numeronym) in the numerical contraction form, such as `a11y`, into its matching words.

It uses the word database found in the `/usr/share/dict/words` file.

## Usage

```
$ numeronym <contractions>
```

For example:

```
$ numeronym a11y

abdominoscopy
abiologically
abominability
aboriginality
absorbability
abstractively
acceleratedly
accendibility
acceptability
accessability
accessibility
accidentality
...
```

```
$ numeronym i18n i18s

=== i18n ===
institutionalisation
institutionalization
intercrystallization
interdifferentiation
internationalisation
internationalization

=== i18s ===
incomprehensibleness
incontrovertibleness
intellectualizations
interdestructiveness

```

## Dependencies

* At build time, the `/usr/share/dict/words` file must exist and contain a valid word database.
