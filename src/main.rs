use std::env;

const WORDS: &str = include_str!("/usr/share/dict/words");

fn main() {
    let args: Vec<String> = env::args().collect();

    let words: Vec<&str> = WORDS.lines().collect();

    match args.len() {
        0 | 1 => {
            eprintln!("Error: Specify at least one numeronym as argument.");
        },
        2 => {
            print_expansions(&words, &args[1]);
        },
        _ => {
            for arg in &args[1..] {
                println!("=== {arg} ===");
                print_expansions(&words, arg);
                println!();
            }
        }
    }
}

fn print_expansions(words: &[&str], arg: &str) {
    if let Some(matching) = words_for_nym(words, arg) {
        println!("{matching}");
    } else {
        eprintln!("Error: No matches.");
    }
}

fn words_for_nym(words: &[&str], nym: &str) -> Option<String> {
    let (head, body_num, last) = split_segments(nym);

    let matching = find_expansions(words, head, body_num, last);

    if matching.is_empty() {
        None
    } else {
        Some(matching.join("\n"))
    }
}

fn split_segments(nym: &str) -> (&str, usize, &str) {
    let length = nym.len();

    let head = &nym[0..1];
    let body = &nym[1..length - 1];
    let body_num: usize = body.parse().unwrap();
    let last = &nym[length - 1..length];

    (head, body_num, last)
}

fn find_expansions<'a>(words: &'a [&str], head: &str, body: usize, last: &str) -> Vec<&'a str> {
    let matching = words
        .iter()
        .filter(|word| word.len() == body + 2)
        .filter(|word| word.starts_with(head))
        .filter(|word| word.ends_with(last))
        .copied();

    matching.collect::<Vec<&str>>()
}
